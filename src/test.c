// tests
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdlib.h>

#define GLOBAL_HEAP_SIZE 10000

void *target_memory_heap;
struct block_header *memory_block;

// Обычное успешное выделение памяти.
void test_malloc_func() {
    printf("\nstarting test: Обычное успешное выделение памяти\n");
    const size_t test_query = 1000;

    debug_heap(stdout, memory_block);

    
    void *mem = _malloc(test_query);

    debug_heap(stdout, memory_block);
    const size_t total_capacity = memory_block->capacity.bytes;

    if (total_capacity == test_query) {
        printf("\nsuccess test: Обычное успешное выделение памяти\n");
    }else{
        printf("\failed test: Обычное успешное выделение памяти\n");
    }

    _free(mem);
}

static size_t get_blocks_num(struct block_header *block) {
    size_t value = 0;

    while (block->next != NULL){
        value++;
        block = block->next;
    }

    return value;
}

// Освобождение одного блока из нескольких выделенных.
void one_block_test() {
    printf("\nstarting test: Освобождение одного блока из нескольких выделенных\n");

    const size_t test_query1 = 2000;
    const size_t test_query2 = 1000;

    void *mem1 = _malloc(test_query1);
    void *mem2 = _malloc(test_query2);

    debug_heap(stdout, memory_block);

    size_t block_number_before = get_blocks_num(memory_block);

    _free(mem2);
   
    size_t block_number_after = get_blocks_num(memory_block);

    _free(mem1);

    if(block_number_before == 2 && block_number_after == 1){
        printf("\nsuccess test: Освобождение одного блока из нескольких выделенных\n");
    }else{
        printf("\n failed test: Освобождение одного блока из нескольких выделенных\n");
        exit(1);
    }

    
    
    
}

// Освобождение двух блоков из нескольких выделенных.
void two_blocks_test() {
    printf("\nstarting test: Освобождение двух блоков из нескольких выделенных\n");

    const size_t test_query1 = 1000;
    const size_t test_query2 = 500;
    const size_t test_query3 = 100;

    void *mem1 = _malloc(test_query1);
    void *mem2 = _malloc(test_query2);
    void *mem3 = _malloc(test_query3);

    debug_heap(stdout, memory_block);

    _free(mem3);
    _free(mem2);

    size_t block_number = get_blocks_num(memory_block);

    _free(mem1);
    

    if (block_number == 1) {
        printf("\nsuccess test: Освобождение двух блоков из нескольких выделенных\n");
    }else{
        printf("\nfailed test: Освобождение двух блоков из нескольких выделенных\n");
        exit(1);
    }
}

// Память закончилась, новый регион памяти расширяет старый.
void extend_memory_test() {
    printf("\nstarting test: Память закончилась, новый регион памяти расширяет старый\n");
    const size_t test_query1 = 9990;
    const size_t test_query2 = 1110;
    const size_t test_query3 = 50;

    void *mem1 = _malloc(test_query1);
    void *mem2 = _malloc(test_query2);
    void *mem3 = _malloc(test_query3);

    debug_heap(stdout, memory_block);

    size_t block_number = get_blocks_num(memory_block);

    if (block_number == 3) {
        printf("\nsuccess test: Память закончилась, новый регион памяти расширяет старый\n");
    }

    _free(mem1);
    _free(mem2);
    _free(mem3);
}

// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void final_test() {
    printf("\nstarting test: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");
    debug_heap(stdout, memory_block);

    const size_t test_query1 = 9990;
    const size_t test_query2 = 1110;

    void* mem1 = _malloc(test_query1);
    debug_heap(stdout, memory_block);

    const size_t busy_region_size = 8000; 
    map_pages(memory_block, busy_region_size, MAP_SHARED);
    debug_heap(stdout, memory_block);

    void* mem2 = _malloc(test_query2);
    debug_heap(stdout, memory_block);

    _free(mem1);
    _free(mem2);

    printf("\nsuccess test: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте\n");
}

// test setup
void start_tests() {
    target_memory_heap = heap_init(GLOBAL_HEAP_SIZE);

    memory_block = (struct block_header *) target_memory_heap;

    const bool init_error_occured = target_memory_heap == NULL || memory_block == NULL;
    if (init_error_occured){
        printf("tests failed in heap_init \n");
        exit(1);
    }

    printf("\nTesting in progress (ㆆ_ㆆ) \n");
    printf("\n>>>>>>>>>>>>>>>>\n\n");

    test_malloc_func();

    one_block_test();

    two_blocks_test();

    extend_memory_test();

    final_test();

    printf("\n<<<<<<<<<<<<<<<<\n");
    printf("\nAll tests were passed ᕙ(`▿´)ᕗ \n");
}
